import React, { Component } from 'react';
import CanvasJSReact from '../../assets/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
 
class CPU2 extends Component {
  	constructor() {
		super();
		this.auxdata = [];
		this.generateDataPoints = this.consumir_api();
	}
	
	generateDataPoints(noOfDps) {
		var xVal = 1, yVal = 100;
		var dps = [];
		for(var i = 0; i < noOfDps; i++) {
			yVal = yVal +  Math.round(5 + Math.random() *(-5-5));
			dps.push({x: xVal,y: yVal});	
			xVal++;
		}
		return dps;
	}


	componentDidMount(){
		setTimeout(()=>{
			this.consumir_api();
		},5000);
	}

	consumir_api(){
		const apiUrl = 'http://104.197.2.171:5000/v1/cpu';
    fetch(apiUrl)
      .then((response) => response.json())
	  .then((data) => 
	  this.auxdata.push(data));
	}
	
	render() {
		const options = {
			theme: "dark2", // "light1", "dark1", "dark2"
			animationEnabled: true,
			zoomEnabled: true,
			title: {
				text: "Estado"
			},
			axisY: {
				includeZero: false
			},
			data: [{
				type: "area",
				dataPoints: this.auxdata.cpu
			}]
		}
		return (
		  <div className="ChartWithZoom">
				<h1>CPU 2</h1>
				<CanvasJSChart options = {options} 
				/* onRef={ref => this.chart = ref} */
				/>
		  </div>
		);
	}
}
export default CPU2;
