import React, { Component } from 'react';
import Comm from './Comment';
import CanvasJSReact from '../../assets/canvasjs.react';
import Select from 'react-select';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
 
class Publicaciones extends Component {

  	constructor() {
		super();
		this.products = [
			{
				'value':'Servidor A',
				'label':'Servidor A'
			},
			{
				'value':'Servidor B',
				'label':'Servidor B'
			},
			{
				'value':'Servidor C',
				'label':'Servidor C'
			},
			{
				'value':'Servidor D',
				'label':'Servidor D'
			},
			{
				'value':'Servidor E',
				'label':'Servidor E'
			},
			{
				'value':'Servidor F',
				'label':'Servidor F'
			}
		];

		this.comments = [];
	}
	
	consumir_api(){
		const apiUrl = 'http://35.193.253.193:5000/v1/ram';
    fetch(apiUrl)
      .then((response) => response.json())
	  .then((data) => 
	  this.comments = data);
	}

	componentDidMount(){
		setTimeout(()=>{
			this.consumir_api();
		},7000);
	}

	
	render() {
		
		return (
		  <div className="ChartWithZoom">
				<h1>Publicaciones</h1>
				<Select name="idcategoria" id="idcategoria" options={this.products} className="form-control">
                  </Select>

				  {this.comments.map(c => (
                    <Comm user={c.autor} comment={c.nota} ></Comm>
                ))}
				
		  </div>
		);
	}
}
export default Publicaciones;
